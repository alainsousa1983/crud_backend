﻿using System.Collections.Generic;
using System.Threading.Tasks;
using App.Core.Interfaces.Services;
using App.Core.Models;
using Microsoft.AspNetCore.Mvc;

namespace CRUDBackEnd.Controllers
{
    /// <summary>
    /// Controller product
    /// </summary>
    [Route("v1/product")]
    [ApiController]
    public class ProductController : ControllerBase
    {

        /// <summary>
        /// Get All product
        /// </summary>
        /// <param name="productService"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("")]
        public async Task<IEnumerable<Product>> GetAll([FromServices] IProductService productService)
        {
            var resul = await productService.GetAllAsync();

            return resul;
        }

        /// <summary>
        /// Get product by Id
        /// </summary>
        /// <param name="productService"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("{id:int}")]
        public async Task<ActionResult<Product>> GetById([FromServices] IProductService productService, int id)
        {
            var resul = await productService.GetAsync(id);

            return resul;
        }

        /// <summary>
        /// Add a new product
        /// </summary>
        /// <param name="productService"></param>
        /// <param name="product"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("")]
        public async Task<int> Add([FromServices] IProductService productService, [FromBody]Product product) 
        {            
            var result = await productService.InsertAsync(product);

            return result;                     
        }

        /// <summary>
        /// Update product
        /// </summary>
        /// <param name="productService"></param>
        /// <param name="product"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("")]
        public async Task<bool> Update([FromServices] IProductService productService, [FromBody]Product product)
        {
            var result = await productService.UpdateAsync(product);

            return result;
        }


        /// <summary>
        /// Delete product
        /// </summary>
        /// <param name="productService"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("")]
        public async Task<bool> Delete([FromServices] IProductService productService, [FromBody]Product product)
        {
            var resul = await productService.DeleteRowAsync(product);

            return resul;
        }


    }
}