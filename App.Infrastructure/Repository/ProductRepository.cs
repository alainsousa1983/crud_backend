﻿using App.Core.Interfaces.Repository;
using App.Core.Models;
using System.Data;
using System.Threading.Tasks;

namespace App.Infrastructure.Repository
{
    public class ProductRepository : DapperRepository<Product>, IProductRepository
    {
        public ProductRepository(IDbConnection dbConnection) : base (dbConnection) { }

        public Task<Product> GetProductByPrice(int minimumPrice, int maximumPrice)
        {            
            throw new System.NotImplementedException();
        }       
    }
}
