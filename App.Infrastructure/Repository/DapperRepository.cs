﻿using App.Core.Interfaces.Repository;
using App.Core.Models;
using Dapper;
using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace App.Infrastructure.Repository
{
    public class DapperRepository<T> : IRepository<T> where T : class
    {
        private readonly IDbConnection _dbConnection;

        public DapperRepository(IDbConnection dbConnection)
        {
            _dbConnection = dbConnection;

        }       

        public Task<IEnumerable<T>> GetAllAsync()
        {
            var result = _dbConnection.GetAllAsync<T>();

            return result;
        }

        public Task<T> GetAsync(int id)
        {
            
            var result = _dbConnection.GetAsync<T>(id);

            return result;
            
        }

        public Task<int> InsertAsync(T entity)
        {
            var result = _dbConnection.InsertAsync(entity);

            return result;
        }

        public Task<bool> UpdateAsync(T entity)
        {
            var result = _dbConnection.UpdateAsync(entity);

            return result;
        }

        public Task<bool> DeleteRowAsync(T entity)
        {
            var result = _dbConnection.DeleteAsync<T>(entity);

            return result;
        }

    }
}
