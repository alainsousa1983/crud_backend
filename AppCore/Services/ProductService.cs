﻿using App.Core.Interfaces.Repository;
using App.Core.Interfaces.Services;
using App.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace App.Core.Services
{
    public class ProductService : IProductService
    {
        private readonly IProductRepository _productRepository;

        public ProductService(IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }

        public Task<bool> DeleteRowAsync(Product product)
        {
            return _productRepository.DeleteRowAsync(product);
        }

        public Task<IEnumerable<Product>> GetAllAsync()
        {
            return _productRepository.GetAllAsync();
        }

        public Task<Product> GetAsync(int id)
        {
            return _productRepository.GetAsync(id);
        }

        public Task<int> InsertAsync(Product product)
        {
            return _productRepository.InsertAsync(product);
        }

        public Task<bool> UpdateAsync(Product product)
        {
            return _productRepository.UpdateAsync(product);
        }        
    }
}
