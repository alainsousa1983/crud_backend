﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace App.Core.Interfaces.Repository
{
    public interface IRepository<T> where T : class
    {
        //Get all
        Task<IEnumerable<T>> GetAllAsync();

        //Get by id
        Task<T> GetAsync(int id);

        //Add
        Task<int> InsertAsync(T entity);

        //Update        
        Task<bool> UpdateAsync(T entity);

        //Delete
        Task<bool> DeleteRowAsync(T entity);
    }
}
