﻿using App.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace App.Core.Interfaces.Repository
{
    public interface IProductRepository : IRepository<Product>
    {
        Task<Product> GetProductByPrice(int minimumPrice, int maximumPrice);
    }
}
