﻿
using App.Core.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace App.Core.Interfaces.Services
{
    public interface IProductService
    {
        //Get all
        Task<IEnumerable<Product>> GetAllAsync();

        //Get by id
        Task<Product> GetAsync(int id);

        //Add
        Task<int> InsertAsync(Product entity);

        //Update        
        Task<bool> UpdateAsync(Product entity);

        //Delete
        Task<bool> DeleteRowAsync(Product product);
    }
}
