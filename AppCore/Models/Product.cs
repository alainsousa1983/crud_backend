﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace App.Core.Models
{
    [Table ("Product")]
    public class Product
    {
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "This property is required")]
        public string Name { get; set; }

        [Required(ErrorMessage = "This property is required")]
        [Range( 1, int.MaxValue, ErrorMessage = "The price most be more then 1" )]
        public decimal Price { get; set; }
    }
}
